# We love community submissions!

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -m "Add some feature"`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request  :)

> English is the universal language nowadays, so please don't create or comment on issues using another language.

## :exclamation: :exclamation: Commit Messages :exclamation: :exclamation:

Please try and make sure there is a relevant [issue ticket](https://gitlab.com/spiculedata/saiku4/saiku4-webframework/issues) available to commit against, this helps us track issues and the code committed to fix the issue/enhancement.

When committing please format your commit message with an issue reference and comment for example:

```sh
git commit -a -m "#12 - Fix bug x by doing y"
```

Where `#12` represents the issue number.
