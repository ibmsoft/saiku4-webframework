/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import axios from 'axios';

// Utils
import { Saiku, Settings } from '../utils';

// Types
import { LOADING_LICENSE, SET_CURRENT_LICENSE } from './types';

export const getLicense = () => dispatch => {
  dispatch(loadingLicense());

  axios
    .get(`${Settings.REST_URL}/api/license`)
    .then(res => {
      const { data } = res;

      dispatch(setCurrentLicense(data));
    })
    .catch(error =>
      Saiku.axiosHandleErrors(
        'actions → licenseActions.js → getLicense()',
        error
      )
    );
};

export const setCurrentLicense = licenseInfo => {
  return {
    type: SET_CURRENT_LICENSE,
    payload: licenseInfo
  };
};

export const loadingLicense = () => {
  return {
    type: LOADING_LICENSE
  };
};
