/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Types
import { QUERY_RESULTSET, QUERY_RESULTSET_ERROR } from './types';

export const queryResultset = data => {
  return {
    type: QUERY_RESULTSET,
    payload: data
  };
};

export const queryResultsetError = error => {
  return {
    type: QUERY_RESULTSET_ERROR,
    payload: error
  };
};
