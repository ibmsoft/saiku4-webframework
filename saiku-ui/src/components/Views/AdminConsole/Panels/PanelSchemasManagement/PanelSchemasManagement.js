/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { Button, Icon, Intent, Position, Tooltip } from '@blueprintjs/core';
import { Input, Table } from 'antd';
import Highlighter from 'react-highlight-words';

// Services
import { SchemaService } from '../../../../../services';

// Actions
import { actionCreators } from '../../../../../actions';

// UI
import { BlockUi, ErrorMessage, Loading, WarningAlert } from '../../../../UI';

// Dialogs
import AddSchemaDialog from './AddSchemaDialog';

// Utils
import { Saiku, Settings } from '../../../../../utils';

// Components
const { Column } = Table;

// Constants
const { SAIKU_COLOR } = Settings;
const ERROR_MSG = 'Error fetching data from schemas';

class PanelSchemasManagement extends Component {
  _isMounted = false;

  state = {
    schemas: [],
    dataSource: [],
    searchText: '',
    itemToDelete: '',
    isOpenAddSchemaDialog: false,
    isOpenDeleteAlert: false,
    loading: true,
    error: false,
    errorMsg: ERROR_MSG
  };

  componentDidMount() {
    this._isMounted = true;
    this.callApiGetSchemas();
  }

  componentWillUnmount() {
    this._isMounted = false;
    SchemaService.cancelRequest();
  }

  callApiGetSchemas = () => {
    this.setState({
      loading: true,
      error: false
    });

    SchemaService.getSchemas()
      .then(res => {
        if (this._isMounted && res.status === 200) {
          const { data } = res;

          this.setState({
            schemas: data,
            dataSource: data.map((schema, index) => {
              return {
                key: index,
                name: schema.name
              };
            }),
            loading: false
          });
        } else {
          if (this._isMounted) {
            this.setState({
              loading: false,
              error: true
            });
          }
        }
      })
      .catch(error => {
        if (this._isMounted) {
          this.setState({
            loading: false,
            error: true
          });
        }
      });
  };

  getTooltipPosition(data) {
    const { searchText } = this.state;

    return data.length === 1 || !isEmpty(searchText)
      ? Position.LEFT_TOP
      : Position.TOP;
  }

  handleDownloadSchemaFile(schemaName) {
    const { requestStart, requestSuccess, requestFailure } = this.props;

    requestStart(<BlockUi message={`Downloading ${schemaName} file...`} />);

    SchemaService.getDownloadSchemaFile(schemaName)
      .then(res => {
        if (res.status === 200) {
          requestSuccess();
        } else {
          requestFailure();
        }
      })
      .catch(error => requestFailure());
  }

  handleDeleteSchema(schemaName) {
    const { requestStart, requestSuccess, requestFailure } = this.props;

    requestStart(<BlockUi message={`Deleting ${schemaName} file...`} />);

    SchemaService.deleteSchema(schemaName)
      .then(res => {
        if (res.status === 200) {
          requestSuccess();

          Saiku.toasts(Position.TOP_RIGHT).show({
            icon: 'tick',
            intent: Intent.SUCCESS,
            message: 'Schema deleted'
          });

          this.callApiGetSchemas();
        } else {
          requestFailure();
          Saiku.toasts(Position.TOP_RIGHT).show({
            icon: 'error',
            intent: Intent.DANGER,
            message: res.statusText
          });
        }
      })
      .catch(error => {
        requestFailure();
        Saiku.toasts(Position.TOP_RIGHT).show({
          icon: 'error',
          intent: Intent.DANGER,
          message: 'Something went wrong'
        });
      });
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => (this.searchInput = node)}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          icon="search"
          text="Search"
          intent={Intent.DANGER}
          style={{ width: 90, marginRight: 8 }}
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          small
        />
        <Button
          text="Reset"
          style={{ width: 90 }}
          onClick={() => this.handleReset(clearFilters)}
          small
        />
      </div>
    ),

    filterIcon: filtered => (
      <Icon
        icon="search"
        iconSize={12}
        style={{ color: filtered ? SAIKU_COLOR : undefined }}
      />
    ),

    onFilter: (value, record) => {
      return record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase());
    },

    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },

    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  handleAddSchemaDialog = () => {
    this.setState(prevState => ({
      isOpenAddSchemaDialog: !prevState.isOpenAddSchemaDialog
    }));
  };

  handleDeleteAlert = name => {
    this.setState(prevState => ({
      itemToDelete: name,
      isOpenDeleteAlert: !prevState.isOpenDeleteAlert
    }));
  };

  renderTable() {
    const {
      dataSource,
      itemToDelete,
      isOpenAddSchemaDialog,
      isOpenDeleteAlert
    } = this.state;

    return (
      <Fragment>
        <div className="sku-btn-group-end m-b-10">
          <Button
            icon="plus"
            text="Add Schema"
            intent={Intent.DANGER}
            onClick={this.handleAddSchemaDialog}
            small
          />
        </div>
        <Table
          dataSource={dataSource}
          pagination={{ pageSize: 10 }}
          scroll={{ y: 240 }}
          size="middle"
        >
          <Column
            title="Schemas"
            dataIndex="name"
            key="name"
            width="30%"
            {...this.getColumnSearchProps('name')}
          />
          <Column
            align="right"
            title="Action"
            key="action"
            render={(text, record) => (
              <span>
                <Tooltip
                  content={<span>Download</span>}
                  position={this.getTooltipPosition(dataSource)}
                >
                  <Button
                    icon="download"
                    intent={Intent.PRIMARY}
                    onClick={this.handleDownloadSchemaFile.bind(
                      this,
                      record.name
                    )}
                    minimal
                  />
                </Tooltip>
                <Tooltip
                  content={<span>Delete</span>}
                  position={this.getTooltipPosition(dataSource)}
                >
                  <Button
                    icon="trash"
                    intent={Intent.DANGER}
                    onClick={this.handleDeleteAlert.bind(this, record.name)}
                    minimal
                  />
                </Tooltip>
              </span>
            )}
          />
        </Table>

        {isOpenAddSchemaDialog && (
          <AddSchemaDialog
            getSchemas={this.callApiGetSchemas}
            onClose={this.handleAddSchemaDialog}
          />
        )}

        {isOpenDeleteAlert && (
          <WarningAlert
            confirmButtonText="Delete"
            icon="trash"
            message={
              <span>
                Are you sure you want to delete the <b>{itemToDelete}</b>{' '}
                schema?
              </span>
            }
            onCancel={this.handleDeleteAlert}
            onConfirm={this.handleDeleteSchema.bind(this, itemToDelete)}
          />
        )}
      </Fragment>
    );
  }

  render() {
    const { loading, error, errorMsg } = this.state;

    return loading ? (
      <Loading className="m-t-10 m-b-10" size={30} center />
    ) : error ? (
      <ErrorMessage text={errorMsg} callApi={this.callApiGetSchemas} />
    ) : (
      this.renderTable()
    );
  }
}

PanelSchemasManagement.propTypes = {
  requestStart: PropTypes.func.isRequired,
  requestSuccess: PropTypes.func.isRequired,
  requestFailure: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  requestStart: message => dispatch(actionCreators.requestStart(message)),
  requestSuccess: () => dispatch(actionCreators.requestSuccess()),
  requestFailure: () => dispatch(actionCreators.requestFailure())
});

export default connect(
  null,
  mapDispatchToProps
)(PanelSchemasManagement);
