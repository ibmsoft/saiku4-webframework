/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { Button, Icon, Intent, Position, Tooltip } from '@blueprintjs/core';
import { Input, Table } from 'antd';
import Highlighter from 'react-highlight-words';

// Services
import { UserService } from '../../../../../services';

// Actions
import { actionCreators } from '../../../../../actions';

// UI
import { BlockUi, ErrorMessage, Loading, WarningAlert } from '../../../../UI';

// Dialogs
import UserFormDialog from './UserFormDialog';

// Utils
import { Saiku, Settings } from '../../../../../utils';

// Components
const { Column } = Table;

// Constants
const { SAIKU_COLOR } = Settings;
const ERROR_MSG = 'Error fetching data from users';

class PanelUserManagement extends Component {
  _isMounted = false;

  state = {
    users: [],
    selectedUserData: {},
    dataSource: [],
    searchText: '',
    itemToDelete: '',
    isEditModeFormDialog: false,
    isOpenUserFormDialog: false,
    isOpenDeleteAlert: false,
    loading: true,
    error: false,
    errorMsg: ERROR_MSG
  };

  componentDidMount() {
    this._isMounted = true;
    this.callApiGetUsers();
  }

  componentWillUnmount() {
    this._isMounted = false;
    UserService.cancelRequest();
  }

  callApiGetUsers = () => {
    this.setState({
      loading: true,
      error: false
    });

    UserService.getUsers()
      .then(res => {
        if (this._isMounted && res.status === 200) {
          const { data } = res;

          this.setState({
            users: data,
            dataSource: data.map(user => {
              return {
                key: user.id,
                username: user.username
              };
            }),
            loading: false
          });
        } else {
          if (this._isMounted) {
            this.setState({
              loading: false,
              error: true
            });
          }
        }
      })
      .catch(error => {
        if (this._isMounted) {
          this.setState({
            loading: false,
            error: true
          });
        }
      });
  };

  getTooltipPosition(data) {
    const { searchText } = this.state;

    return data.length === 1 || !isEmpty(searchText)
      ? Position.LEFT_TOP
      : Position.TOP;
  }

  getUser(id) {
    return this.state.users.find(user => user.id === id);
  }

  handleEditUser(id) {
    const selectedUserData = this.getUser(id);
    this.handleUserFormDialog(true, selectedUserData);
  }

  handleDeleteUser(user) {
    const { requestStart, requestSuccess, requestFailure } = this.props;
    const { id, username } = user;

    requestStart(<BlockUi message={`Deleting ${username} file...`} />);

    UserService.deleteUser(id)
      .then(res => {
        if (res.status === 200) {
          requestSuccess();

          Saiku.toasts(Position.TOP_RIGHT).show({
            icon: 'tick',
            intent: Intent.SUCCESS,
            message: 'User deleted'
          });

          this.callApiGetUsers();
        } else {
          requestFailure();
          Saiku.toasts(Position.TOP_RIGHT).show({
            icon: 'error',
            intent: Intent.DANGER,
            message: res.statusText
          });
        }
      })
      .catch(error => {
        requestFailure();
        Saiku.toasts(Position.TOP_RIGHT).show({
          icon: 'error',
          intent: Intent.DANGER,
          message: 'Something went wrong'
        });
      });
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => (this.searchInput = node)}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={event =>
            setSelectedKeys(event.target.value ? [event.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          icon="search"
          text="Search"
          intent={Intent.DANGER}
          style={{ width: 90, marginRight: 8 }}
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          small
        />
        <Button
          text="Reset"
          style={{ width: 90 }}
          onClick={() => this.handleReset(clearFilters)}
          small
        />
      </div>
    ),

    filterIcon: filtered => (
      <Icon
        icon="search"
        iconSize={12}
        style={{ color: filtered ? SAIKU_COLOR : undefined }}
      />
    ),

    onFilter: (value, record) => {
      return record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase());
    },

    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },

    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        textToHighlight={text.toString()}
        autoEscape
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  handleUserFormDialog = (isEditMode = false, selectedUserData) => {
    this.setState(prevState => ({
      selectedUserData,
      isEditModeFormDialog: isEditMode,
      isOpenUserFormDialog: !prevState.isOpenUserFormDialog
    }));
  };

  handleDeleteAlert = user => {
    this.setState(
      prevState => ({
        isOpenDeleteAlert: !prevState.isOpenDeleteAlert
      }),
      () => {
        if (user) {
          this.setState({
            itemToDelete: { id: user.key, username: user.username }
          });
        }
      }
    );
  };

  renderTable() {
    const {
      selectedUserData,
      dataSource,
      itemToDelete,
      isEditModeFormDialog,
      isOpenUserFormDialog,
      isOpenDeleteAlert
    } = this.state;

    return (
      <Fragment>
        <div className="sku-btn-group-end m-b-10">
          <Button
            icon="plus"
            text="Add User"
            intent={Intent.DANGER}
            onClick={this.handleUserFormDialog.bind(this, false)}
            small
          />
        </div>
        <Table
          dataSource={dataSource}
          pagination={{ pageSize: 10 }}
          scroll={{ y: 240 }}
          size="middle"
        >
          <Column
            title="Users"
            dataIndex="username"
            key="username"
            width="20%"
            {...this.getColumnSearchProps('username')}
          />
          <Column
            align="right"
            title="Action"
            key="action"
            render={(text, record) => (
              <span>
                <Tooltip
                  content={<span>Edit</span>}
                  position={this.getTooltipPosition(dataSource)}
                >
                  <Button
                    icon="edit"
                    intent={Intent.PRIMARY}
                    onClick={this.handleEditUser.bind(this, record.key)}
                    minimal
                  />
                </Tooltip>
                <Tooltip
                  content={<span>Delete</span>}
                  position={this.getTooltipPosition(dataSource)}
                >
                  <Button
                    icon="trash"
                    intent={Intent.DANGER}
                    onClick={this.handleDeleteAlert.bind(this, record)}
                    minimal
                  />
                </Tooltip>
              </span>
            )}
          />
        </Table>

        {isOpenUserFormDialog && (
          <UserFormDialog
            editMode={isEditModeFormDialog}
            userData={selectedUserData}
            getUsers={this.callApiGetUsers}
            onClose={this.handleUserFormDialog.bind(this, false)}
          />
        )}

        {isOpenDeleteAlert && (
          <WarningAlert
            confirmButtonText="Delete"
            icon="trash"
            message={
              <span>
                Are you sure you want to delete the{' '}
                <b>{itemToDelete.username}</b> user?
              </span>
            }
            onCancel={this.handleDeleteAlert}
            onConfirm={this.handleDeleteUser.bind(this, itemToDelete)}
          />
        )}
      </Fragment>
    );
  }

  render() {
    const { loading, error, errorMsg } = this.state;

    return loading ? (
      <Loading className="m-t-10 m-b-10" size={30} center />
    ) : error ? (
      <ErrorMessage text={errorMsg} callApi={this.callApiGetUsers} />
    ) : (
      this.renderTable()
    );
  }
}

PanelUserManagement.propTypes = {
  requestStart: PropTypes.func.isRequired,
  requestSuccess: PropTypes.func.isRequired,
  requestFailure: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  requestStart: message => dispatch(actionCreators.requestStart(message)),
  requestSuccess: () => dispatch(actionCreators.requestSuccess()),
  requestFailure: () => dispatch(actionCreators.requestFailure())
});

export default connect(
  null,
  mapDispatchToProps
)(PanelUserManagement);
