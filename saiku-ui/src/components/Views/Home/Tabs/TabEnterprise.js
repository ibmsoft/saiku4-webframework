/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

// UI
import { ExternalLink, Logo } from '../../../UI';

// Utils
import { Links } from '../../../../utils';

// Styles
const styles = theme => ({
  root: {
    padding: '20px',
    flexGrow: 1
  },
  skuSectionTitle: {
    color: '#6d6e71',
    fontWeight: 'bold',
    marginBottom: '12px'
  },
  skuLink: {
    color: '#00f',
    outline: 'none',
    '&:hover': {
      textDecoration: 'underline !important'
    }
  }
});

const TabEnterprise = ({ classes }) => {
  return (
    <div className={classes.root}>
      <Grid>
        <Row>
          <Col xs>
            {/* Section Logo */}
            <Row>
              <Col xs>
                <Logo width={250} height={125} full />
              </Col>
            </Row>
            {/* Section Title */}
            <Row>
              <Col xs>
                <Typography
                  className={classes.skuSectionTitle}
                  variant="button"
                >
                  Enterprise
                </Typography>
              </Col>
            </Row>
            {/* Section Enterprise */}
            <Row>
              <Col xs>
                <Typography className="m-b-10" align="justify">
                  Saiku Enterprise is our fully supported and tested server and
                  Pentaho plugin system. Buy Saiku Enterprise from as little as
                  $15 per user per month and enjoy the addtional features Saiku
                  Enterprise has to offer.
                </Typography>
                <Typography align="justify">
                  To find out more visit our{' '}
                  <ExternalLink
                    className={classes.skuLink}
                    href={Links.meteorite.home}
                  >
                    site
                  </ExternalLink>{' '}
                  or{' '}
                  <a
                    className={classes.skuLink}
                    href={`mailto:${Links.meteorite.email.info}?subject=Saiku%20Enterprise`}
                  >
                    schedule a call
                  </a>{' '}
                  with one of us and we can show you why you should choose Saiku
                  Enterprise!
                </Typography>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </div>
  );
};

TabEnterprise.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TabEnterprise);
