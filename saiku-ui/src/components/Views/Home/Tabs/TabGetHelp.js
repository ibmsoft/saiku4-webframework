/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

// UI
import { ExternalLink, Logo } from '../../../UI';

// Utils
import { Links } from '../../../../utils';

// Styles
const styles = theme => ({
  root: {
    padding: '20px',
    flexGrow: 1
  },
  skuSectionTitle: {
    color: '#6d6e71',
    fontWeight: 'bold',
    marginBottom: '12px'
  },
  skuSectionSubTitle: {
    fontSize: '1rem',
    color: theme.palette.secondary.main,
    fontWeight: 'bold',
    marginBottom: '12px'
  },
  skuLink: {
    color: '#00f',
    outline: 'none',
    '&:hover': {
      textDecoration: 'underline !important'
    }
  }
});

const TabGetHelp = ({ classes }) => {
  return (
    <div className={classes.root}>
      <Grid>
        <Row>
          <Col xs>
            {/* Section Logo */}
            <Row>
              <Col xs>
                <Logo width={250} height={125} full />
              </Col>
            </Row>
            {/* Section Title */}
            <Row>
              <Col xs>
                <Typography
                  className={classes.skuSectionTitle}
                  variant="button"
                >
                  Help
                </Typography>
              </Col>
            </Row>
            {/* Section Help */}
            <Row className="m-b-30">
              <Col xs>
                <Typography align="justify">
                  We provide Training, Consulting and Support to ensure you get
                  the most from Saiku and your data. Our services cover all
                  aspects of data analysis including data strategy, design,
                  architecture, deployment and application/software support.
                </Typography>
              </Col>
            </Row>
            {/* Section Items */}
            <Row>
              <Col xs>
                <Typography
                  className={classes.skuSectionSubTitle}
                  align="center"
                  variant="button"
                >
                  Tutorials
                </Typography>
                <Typography align="center">
                  See how to get started with Saiku with these step by step{' '}
                  <ExternalLink
                    className={classes.skuLink}
                    href={Links.saiku.docs.tutorials}
                  >
                    tutorials
                  </ExternalLink>
                  .
                </Typography>
              </Col>
              <Col xs>
                <Typography
                  className={classes.skuSectionSubTitle}
                  align="center"
                  variant="button"
                >
                  Docs
                </Typography>
                <Typography align="center">
                  Why not try our new{' '}
                  <ExternalLink
                    className={classes.skuLink}
                    href={Links.saiku.docs.home}
                  >
                    documentation site
                  </ExternalLink>{' '}
                  for community documentation.
                </Typography>
              </Col>
              <Col xs>
                <Typography
                  className={classes.skuSectionSubTitle}
                  align="center"
                  variant="button"
                >
                  Support
                </Typography>
                <Typography align="center">
                  If you require more,{' '}
                  <ExternalLink
                    className={classes.skuLink}
                    href={`mailto:${Links.meteorite.email.info}`}
                  >
                    contact us
                  </ExternalLink>{' '}
                  for support!
                </Typography>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </div>
  );
};

TabGetHelp.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TabGetHelp);
