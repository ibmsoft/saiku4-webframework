/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Button, Collapse, Icon, Intent } from '@blueprintjs/core';

// Utils
import { Settings } from '../../../utils';

// Constants
const LOGIN_ICON_SIZE = 14;

export default ({ isOpen = false, loginAs }) => {
  return (
    <Collapse isOpen={isOpen} transitionDuration={200} keepChildrenMounted>
      <div className="sku-evaluation-panel">
        <Grid fluid>
          <Row>
            <Col xs>
              <div className="sku-role-panel">
                <div className="sku-login-role">Administrator</div>
                <ul className="sku-login-data">
                  <li>
                    <Icon icon="person" iconSize={LOGIN_ICON_SIZE} />{' '}
                    {Settings.LOGIN_ROLE_ADMIN.username}
                  </li>
                  <li>
                    <Icon icon="key" iconSize={LOGIN_ICON_SIZE} />{' '}
                    {Settings.LOGIN_ROLE_ADMIN.password}
                  </li>
                </ul>
                <Button
                  className="button-right"
                  text="Go"
                  intent={Intent.DANGER}
                  onClick={() =>
                    loginAs(
                      Settings.LOGIN_ROLE_ADMIN.username,
                      Settings.LOGIN_ROLE_ADMIN.password
                    )
                  }
                  small
                  fill
                />
              </div>
            </Col>
            <Col xs>
              <div className="sku-role-panel">
                <div className="sku-login-role">Business User</div>
                <ul className="sku-login-data">
                  <li>
                    <Icon icon="person" iconSize={LOGIN_ICON_SIZE} />{' '}
                    {Settings.LOGIN_ROLE_USER.username}
                  </li>
                  <li>
                    <Icon icon="key" iconSize={LOGIN_ICON_SIZE} />{' '}
                    {Settings.LOGIN_ROLE_USER.password}
                  </li>
                </ul>
                <Button
                  className="button-right"
                  text="Go"
                  intent={Intent.DANGER}
                  onClick={() =>
                    loginAs(
                      Settings.LOGIN_ROLE_USER.username,
                      Settings.LOGIN_ROLE_USER.password
                    )
                  }
                  small
                  fill
                />
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    </Collapse>
  );
};
