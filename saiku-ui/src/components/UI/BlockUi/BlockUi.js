/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { Intent, Spinner } from '@blueprintjs/core';

// UI
import { Logo } from '../';

// Styles
import './BlockUi.css';

const BlockUi = ({ message, action }) => {
  return (
    <div className="sku-blockui-container">
      <Logo width={32} height={32} small />
      <div className="sku-blockui-content">
        <Spinner intent={Intent.DANGER} size={Spinner.SIZE_SMALL} /> &nbsp;{' '}
        {message} &nbsp; {action}
      </div>
    </div>
  );
};

BlockUi.propTypes = {
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  action: PropTypes.node
};

BlockUi.defaultProps = {
  message: 'Loading, please wait...'
};

export default BlockUi;
