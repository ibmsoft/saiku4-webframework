/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert, Intent } from '@blueprintjs/core';

// Actions
import { actionCreators } from '../../../actions';

const RefreshCubes = props => {
  const { refreshDatasources, onCancel } = props;

  const handleRefreshDatasources = () => {
    refreshDatasources();
    onCancel();
  };

  return (
    <Alert
      canEscapeKeyCancel={true}
      canOutsideClickCancel={true}
      cancelButtonText="Cancel"
      confirmButtonText="Refresh"
      icon="info-sign"
      intent={Intent.DANGER}
      isOpen={true}
      onCancel={onCancel}
      onConfirm={handleRefreshDatasources}
    >
      <p>You are about to refresh cubes and clear cache!</p>
    </Alert>
  );
};

RefreshCubes.propTypes = {
  refreshDatasources: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  refreshDatasources: () => dispatch(actionCreators.refreshDatasources())
});

export default connect(
  null,
  mapDispatchToProps
)(RefreshCubes);
