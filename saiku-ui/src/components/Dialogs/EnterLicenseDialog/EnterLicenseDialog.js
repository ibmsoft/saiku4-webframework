/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Callout,
  Classes,
  Code,
  Dialog,
  FormGroup,
  InputGroup,
  Intent,
  Position,
  TextArea
} from '@blueprintjs/core';
import { FormValidation } from 'calidation';

// Services
import { LicenseService } from '../../../services';

// UI
import { ExternalLink } from '../../UI';

// Utils
import { Links, Saiku } from '../../../utils';

// Styles
import './EnterLicenseDialog.css';

class EnterLicenseDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };

    this.formValidationConfig = {
      name: {
        isRequired: 'Name field is required'
      },
      company: {
        isRequired: 'Company field is required'
      },
      key: {
        isRequired: 'License key field is required'
      }
    };
  }

  handleSubmit = ({ fields, errors, isValid }) => {
    if (isValid) {
      const { toastPosition, hideErrorContent, onClose } = this.props;
      const licenseData = fields;

      this.setState({ loading: true });

      LicenseService.postLicense(licenseData)
        .then(res => {
          const { message } = res.data;

          this.setState({ loading: false });

          if (res.status === 200) {
            Saiku.toasts(Position[toastPosition]).show({
              icon: 'tick',
              intent: Intent.SUCCESS,
              message
            });

            // Hide error content on login page
            if (typeof hideErrorContent === 'function') {
              hideErrorContent(true);
            }

            onClose();
          } else {
            Saiku.toasts(Position[toastPosition]).show({
              icon: 'error',
              intent: Intent.DANGER,
              message
            });
          }
        })
        .catch(error =>
          Saiku.toasts(Position[toastPosition]).show({
            icon: 'error',
            intent: Intent.DANGER,
            message: 'Error validating the license!'
          })
        );
    }
  };

  renderInfo() {
    return (
      <Callout icon="warning-sign" intent={Intent.WARNING} title="Important:">
        Your <Code>name</Code> and <Code>company</Code> have to match the one's
        used to download the license!
      </Callout>
    );
  }

  renderForm() {
    const { onClose } = this.props;
    const { loading } = this.state;

    return (
      <FormValidation
        onSubmit={this.handleSubmit}
        config={this.formValidationConfig}
        style={{ margin: 0 }}
      >
        {({ fields, errors, submitted }) => (
          <Fragment>
            <div className={Classes.DIALOG_BODY}>
              {this.renderInfo()}

              <div className="sku-enter-license-dialog">
                <p>
                  Fill out the license form below. You can purchase a license
                  from:{' '}
                  <ExternalLink href={Links.saiku.license}>
                    {Links.saiku.license}
                  </ExternalLink>
                </p>
              </div>
              <FormGroup
                label="Name"
                labelFor="name"
                intent={submitted && errors.name ? Intent.DANGER : Intent.NONE}
                helperText={submitted && errors.name ? errors.name : ''}
              >
                <InputGroup
                  name="name"
                  intent={
                    submitted && errors.name ? Intent.DANGER : Intent.NONE
                  }
                  autoFocus
                />
              </FormGroup>
              <FormGroup
                label="Company"
                labelFor="company"
                intent={
                  submitted && errors.company ? Intent.DANGER : Intent.NONE
                }
                helperText={submitted && errors.company ? errors.company : ''}
              >
                <InputGroup
                  name="company"
                  intent={
                    submitted && errors.company ? Intent.DANGER : Intent.NONE
                  }
                />
              </FormGroup>
              <FormGroup
                label="License Key"
                labelFor="key"
                intent={submitted && errors.key ? Intent.DANGER : Intent.NONE}
                helperText={submitted && errors.key ? errors.key : ''}
              >
                <TextArea
                  name="key"
                  intent={submitted && errors.key ? Intent.DANGER : Intent.NONE}
                  fill
                  large
                />
              </FormGroup>
            </div>
            <div className={Classes.DIALOG_FOOTER}>
              <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                <Button
                  type="submit"
                  text="Use License"
                  intent={Intent.DANGER}
                  loading={loading}
                />
                <Button text="Close" onClick={onClose} />
              </div>
            </div>
          </Fragment>
        )}
      </FormValidation>
    );
  }

  render() {
    const { onClose } = this.props;

    return (
      <Dialog
        title="Enter License"
        icon="key"
        canOutsideClickClose={false}
        isOpen={true}
        onClose={onClose}
      >
        {this.renderForm()}
      </Dialog>
    );
  }
}

EnterLicenseDialog.propTypes = {
  toastPosition: PropTypes.oneOf([
    'TOP_LEFT',
    'TOP',
    'TOP_RIGHT',
    'BOTTOM_LEFT',
    'BOTTOM',
    'BOTTOM_RIGHT'
  ]),
  hideErrorContent: PropTypes.func,
  onClose: PropTypes.func.isRequired
};

EnterLicenseDialog.defaultProps = {
  toastPosition: 'TOP'
};

export default EnterLicenseDialog;
