/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import { cloneDeep, findIndex } from 'lodash';

// Utils
import { Settings } from '../../../../../utils';

export const dragDisableMeasure = (
  measuresDataState,
  draggableSource,
  options,
  isDragDisabled = true
) => {
  const measuresDataStateClone = cloneDeep(measuresDataState);
  const measureGroupName = options.data;
  const measuresClone = cloneDeep(measuresDataStateClone[measureGroupName]);
  const measureIndex = draggableSource.index;

  measuresClone[measureIndex]['isDragDisabled'] = isDragDisabled;

  const newState = {
    ...measuresDataState,
    [measureGroupName]: measuresClone
  };

  return newState;
};

export const dragEnableMeasure = (
  measuresDataState,
  measureName,
  isDragDisabled = false
) => {
  const measuresDataStateClone = cloneDeep(measuresDataState);
  let measureGroupName;
  let measureIndex = -1;

  Object.keys(measuresDataStateClone).some(groupName => {
    measureIndex = findIndex(measuresDataStateClone[groupName], {
      name: measureName,
      measureGroup: groupName
    });

    if (measureIndex !== -1) {
      measureGroupName = groupName;

      return true;
    }

    return false;
  });

  const measuresClone = cloneDeep(measuresDataStateClone[measureGroupName]);

  measuresClone[measureIndex]['isDragDisabled'] = isDragDisabled;

  const newState = {
    ...measuresDataState,
    [measureGroupName]: measuresClone
  };

  return newState;
};

export const copyMeasure = (axesStates, measureData, droppableDestination) => {
  const axesStatesClone = cloneDeep(axesStates);
  const axisName = droppableDestination.droppableId;
  const destinationClone = cloneDeep(axesStatesClone[axisName].items);

  destinationClone.splice(droppableDestination.index, 0, { ...measureData });

  const axis = {
    ...axesStates[axisName],
    items: destinationClone
  };

  const newState = {
    ...axesStates,
    [axisName]: {
      ...axis
    }
  };

  return newState;
};

export const reorderMeasure = (
  axesStates,
  measureItems,
  draggableSource,
  droppableDestination
) => {
  const axisName = droppableDestination.droppableId;
  const startIndex = draggableSource.index;
  const endIndex = droppableDestination.index;
  const measureItemsClone = cloneDeep(measureItems);
  const [removed] = measureItemsClone.splice(startIndex, 1);

  measureItemsClone.splice(endIndex, 0, removed);

  const axis = {
    ...axesStates[axisName],
    items: measureItemsClone
  };

  const newState = {
    ...axesStates,
    [axisName]: {
      ...axis
    }
  };

  return newState;
};

export const removeMeasure = (axesStates, measureItems, draggableSource) => {
  const axisName = draggableSource.draggableId;
  const oldIndex = draggableSource.index;
  const measureItemsClone = cloneDeep(measureItems);

  measureItemsClone.splice(oldIndex, 1);

  const axis = {
    ...axesStates[axisName],
    items: measureItemsClone
  };

  const newState = {
    ...axesStates,
    [axisName]: {
      ...axis
    }
  };

  return newState;
};

export const setMeasuresDetails = details => {
  let newState = Settings.MEASURES_QUERY_DETAILS;

  if (details !== 'reset') {
    const location = details.split('_')[0];
    const axis = details.split('_')[1];

    newState = { location, axis };
  }

  return newState;
};

export const clearMeasures = (axesStates, axisName) => {
  const axis = {
    ...axesStates[axisName],
    items: []
  };

  const newState = {
    ...axesStates,
    [axisName]: {
      ...axis
    }
  };

  return newState;
};
