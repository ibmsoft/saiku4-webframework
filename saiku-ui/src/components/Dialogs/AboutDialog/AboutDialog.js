/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Button, Classes, Dialog } from '@blueprintjs/core';

// UI
import { ExternalLink, Logo } from '../../UI';

// Images
import meteoriteLogo from '../../../images/meteorite/logo-small.png';

// Utils
import { Links, Settings } from '../../../utils';

// Styles
import './AboutDialog.css';

class AboutDialog extends Component {
  renderLicenseInfo() {
    const { license } = this.props;

    return (
      <ul>
        <li className="sku-label">
          Type: <span className="sku-item">{license.licenseType}</span>
        </li>
        <li className="sku-label">
          Expires: <span className="sku-item">{license.expiration}</span>
        </li>
        <li className="sku-label">
          Number of users: <span className="sku-item">{license.userLimit}</span>
        </li>
        <li className="sku-label">
          Licensed to: <span className="sku-item">{license.name}</span> -{' '}
          <span className="sku-item">{license.company}</span>
        </li>
        <li>
          <ExternalLink href={Links.saiku.license}>
            Order more licenses here
          </ExternalLink>
        </li>
      </ul>
    );
  }

  render() {
    const { onClose } = this.props;

    return (
      <Dialog
        title="About"
        icon="info-sign"
        canOutsideClickClose={false}
        isOpen={true}
        onClose={onClose}
      >
        <div className={Classes.DIALOG_BODY}>
          <Grid fluid>
            <Row>
              <Col xs={2}>
                <Logo width={50} height={50} small />
              </Col>
              <Col xs>
                <div className="sku-about-us">
                  <div className="sku-header">
                    <span>{Settings.VERSION}</span>
                    <ExternalLink href={Links.meteorite.home}>
                      {Links.meteorite.home}
                    </ExternalLink>
                  </div>
                  <div className="sku-license-info">
                    <h3>License Info</h3>
                    {this.renderLicenseInfo()}
                  </div>
                  <div className="sku-footer">
                    <span>
                      Want to help?{' '}
                      <ExternalLink href={Links.saiku.gitlab}>
                        Contribute to the code!
                      </ExternalLink>
                    </span>
                    <span>
                      Powered by{' '}
                      <ExternalLink href={Links.meteorite.home}>
                        <img
                          src={meteoriteLogo}
                          width="20"
                          alt="Meteorite BI logo"
                        />
                      </ExternalLink>{' '}
                      <ExternalLink href={Links.meteorite.services.consulting}>
                        www.meteorite.bi
                      </ExternalLink>
                    </span>
                  </div>
                </div>
              </Col>
            </Row>
          </Grid>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Button text="Close" onClick={onClose} />
          </div>
        </div>
      </Dialog>
    );
  }
}

AboutDialog.propTypes = {
  license: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.licenseInfo
});

export default connect(mapStateToProps)(AboutDialog);
