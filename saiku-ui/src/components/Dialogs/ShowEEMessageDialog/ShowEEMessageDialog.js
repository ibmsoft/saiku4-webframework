/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Button, Classes, Dialog } from '@blueprintjs/core';

// UI
import { ExternalLink, Logo } from '../../UI';

// Utils
import { Links } from '../../../utils';

// Styles
import './ShowEEMessageDialog.css';

export default ({ onClose }) => {
  return (
    <Dialog
      className="sku-show-ee-message-dialog bp3-dark"
      title="Saiku Enterprise"
      icon="office"
      canOutsideClickClose={false}
      isOpen={true}
      onClose={onClose}
    >
      <div className={Classes.DIALOG_BODY}>
        <Grid fluid>
          <Row>
            <Col xs={2}>
              <Logo width={50} height={50} small />
            </Col>
            <Col xs>
              <div className="bp3-text-large">
                <p>
                  This is an Enterprise feature, please click{' '}
                  <ExternalLink href={Links.saiku.license}>here</ExternalLink>{' '}
                  to purchase a license.
                </p>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button text="Close" onClick={onClose} />
        </div>
      </div>
    </Dialog>
  );
};
