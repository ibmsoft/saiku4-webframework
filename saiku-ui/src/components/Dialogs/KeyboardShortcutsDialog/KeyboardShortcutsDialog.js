/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Button, Classes, Dialog, KeyCombo } from '@blueprintjs/core';
import { Descriptions } from 'antd';

// Icons
import keyboardIcon from '../../../images/icons/keyboard.svg';

// Components
const { Item } = Descriptions;

// Constants
const ITEM_SPAN = 3;

export default ({ onClose }) => {
  return (
    <Dialog
      title="Keyboard Shortcuts"
      icon={
        <img
          className="bp3-icon"
          src={keyboardIcon}
          width="25"
          alt="Keyboard icon"
        />
      }
      canOutsideClickClose={false}
      isOpen={true}
      style={{ width: '600px' }}
      onClose={onClose}
    >
      <div className={Classes.DIALOG_BODY}>
        <Grid fluid>
          <Row>
            <Col xs>
              <Descriptions title="Saiku Query" size="small">
                <Item label="Open Query" span={ITEM_SPAN}>
                  <KeyCombo combo="Ctrl + O" />
                </Item>
                <Item label="Save Query" span={ITEM_SPAN}>
                  <KeyCombo combo="Ctrl + S" />
                </Item>
                <Item label="Save Query as" span={ITEM_SPAN}>
                  <KeyCombo combo="Ctrl + Shift + S" />
                </Item>
                <Item label="Reset Query" span={ITEM_SPAN}>
                  <KeyCombo combo="Ctrl + Q" />
                </Item>
                <Item label="Query Designer" span={ITEM_SPAN}>
                  <KeyCombo combo="Ctrl + D" />
                </Item>
              </Descriptions>
            </Col>
            <Col xs>
              <Descriptions title="Browser Commands" size="small">
                <Item label="Zoom In" span={ITEM_SPAN}>
                  <KeyCombo combo="Ctrl + plus" />
                </Item>
                <Item label="Zoom Out" span={ITEM_SPAN}>
                  <KeyCombo combo="Ctrl + minus" />
                </Item>
              </Descriptions>
            </Col>
          </Row>
        </Grid>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button text="Close" onClick={onClose} />
        </div>
      </div>
    </Dialog>
  );
};
