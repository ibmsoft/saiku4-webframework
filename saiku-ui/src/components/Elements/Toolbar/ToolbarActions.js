/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { PureComponent } from 'react';
import { NavLink } from 'react-router-dom';
import { Icon } from '@blueprintjs/core';

// Dialogs
import { ShowEEMessageDialog } from '../../Dialogs';

// Styles
import './ToolbarActions.css';

class ToolbarActions extends PureComponent {
  state = {
    isShowEEMessageDialog: false
  };

  toggleActionsBodyClass = () => {
    document.body.classList.toggle('toolbar-actions-open');
  };

  handleShowEEMessageDialog = () => {
    this.setState(
      prevState => ({
        isShowEEMessageDialog: !prevState.isShowEEMessageDialog
      }),
      () => this.toggleActionsBodyClass()
    );
  };

  render() {
    const { isShowEEMessageDialog } = this.state;

    return (
      <div className="toolbar-actions">
        <div className="toolbar-slide-overlay" />
        <div className="toolbar-slide-inner">
          <div
            className="toolbar-slide-close"
            onClick={this.toggleActionsBodyClass}
          >
            <Icon icon="cross" />
          </div>
          <div className="toolbar-slide-content">
            <strong>Create New</strong>
            <ul>
              <li>
                <NavLink to="/workspace" onClick={this.toggleActionsBodyClass}>
                  <Icon icon="insert" /> <span>Saiku Query</span>
                </NavLink>
              </li>
              <li>
                <button
                  type="button"
                  className="link-button"
                  onClick={this.handleShowEEMessageDialog}
                >
                  <Icon icon="dashboard" /> <span>Saiku Dashboard</span>
                </button>
              </li>
              <li>
                <button type="button" className="link-button">
                  <Icon icon="document" /> <span>Saiku Report</span>
                </button>
              </li>
            </ul>
          </div>
        </div>

        {isShowEEMessageDialog && (
          <ShowEEMessageDialog onClose={this.handleShowEEMessageDialog} />
        )}
      </div>
    );
  }
}

export default ToolbarActions;
