/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import { defer } from 'lodash';

// Renders
import SaikuChartRenderer from '../../../renders/SaikuChartRenderer';

// Utils
import { Settings } from '../../../utils';

class Chart extends Component {
  constructor(props) {
    super(props);

    this.saikuChartRenderer = {};
  }

  componentDidMount() {
    const { hasRun, queryResultset } = this.props.queryResultset;

    this.$rootNode = $(this.rootNode);

    this.saikuChartRenderer = new SaikuChartRenderer(null, {
      htmlObject: this.$rootNode,
      zoom: true,
      adjustSizeTo: '.sku-workspace-results'
    });

    if (hasRun) {
      this.saikuChartRenderer.data = {};
      this.saikuChartRenderer.data.resultset = [];
      this.saikuChartRenderer.data.metadata = [];
      this.$rootNode.find('.sku-canvas-wrapper').hide();

      // Render the chart without blocking the UI thread
      defer(this.receiveData, queryResultset);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const queryResultsetProps = this.props.queryResultset;
    const queryResultsetNextProps = nextProps.queryResultset;

    const typeProps = this.props.type;
    const typeNextProps = nextProps.type;

    return (
      queryResultsetProps.queryResultset.width !==
        queryResultsetNextProps.queryResultset.width ||
      queryResultsetProps.queryResultset.height !==
        queryResultsetNextProps.queryResultset.height ||
      queryResultsetProps.queryResultset.runtime !==
        queryResultsetNextProps.queryResultset.runtime ||
      typeProps !== typeNextProps
    );
  }

  componentWillUpdate(nextProps, nextState) {
    const { queryResultset } = nextProps.queryResultset;

    // Render the chart without blocking the UI thread
    defer(this.receiveData, queryResultset);
  }

  componentWillUnmount() {
    this.$rootNode.empty();
  }

  receiveData = data => {
    const { type } = this.props;

    this.saikuChartRenderer.processDataTree({ data }, true, true);
    this.saikuChartRenderer.switchChart(type, { chartDefinition: null });
  };

  render() {
    return (
      <div ref={node => (this.rootNode = node)} className="sku-chart-wrapper" />
    );
  }
}

Chart.propTypes = {
  type: PropTypes.string,
  queryResultset: PropTypes.object.isRequired
};

Chart.defaultProps = {
  type: Settings.DEFAULT_CHART_TYPE
};

export default Chart;
