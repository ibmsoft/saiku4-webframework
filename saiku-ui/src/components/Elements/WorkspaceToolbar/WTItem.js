/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React from 'react';
import { Button, Position, Tooltip } from '@blueprintjs/core';

export default ({
  icon = 'square',
  content = 'Item Name',
  active = false,
  disabled = false,
  action
}) => {
  const handleAction = () => {
    if (typeof action === 'function') {
      action();
    }
  };

  return disabled ? (
    <Button
      icon={icon}
      active={active}
      disabled={disabled}
      onClick={handleAction}
      minimal
    />
  ) : (
    <Tooltip content={content} position={Position.BOTTOM}>
      <Button
        icon={icon}
        active={active}
        disabled={disabled}
        onClick={handleAction}
        minimal
      />
    </Tooltip>
  );
};
