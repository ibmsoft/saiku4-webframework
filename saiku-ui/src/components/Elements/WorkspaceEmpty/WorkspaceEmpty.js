/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import isEqual from 'react-fast-compare';
import { Button, ButtonGroup, NonIdealState } from '@blueprintjs/core';

// Dialogs
import { CubesDialog, QueryDesignerDialog } from '../../Dialogs';

class WorkspaceEmpty extends Component {
  state = {
    isOpenCubesDialog: false,
    isOpenQueryDesignerDialog: false
  };

  shouldComponentUpdate(nextProps, nextState) {
    return (
      !isEqual(this.props.query, nextProps.query) ||
      !isEqual(this.props.queryResultset, nextProps.queryResultset) ||
      this.state.isOpenCubesDialog !== nextState.isOpenCubesDialog ||
      this.state.isOpenQueryDesignerDialog !==
        nextState.isOpenQueryDesignerDialog
    );
  }

  handleCubesDialog = () => {
    this.setState(prevState => ({
      isOpenCubesDialog: !prevState.isOpenCubesDialog
    }));
  };

  handleOpenQueryDialog = () => {
    this.props.openQuery();
  };

  handleQueryDesignerDialog = () => {
    this.setState(prevState => ({
      isOpenQueryDesignerDialog: !prevState.isOpenQueryDesignerDialog
    }));
  };

  render() {
    const { query, queryResultset } = this.props;
    const { isOpenCubesDialog, isOpenQueryDesignerDialog } = this.state;

    return query && !query.cube ? (
      <Fragment>
        <NonIdealState
          icon="page-layout"
          title="Empty Workspace"
          action={
            <ButtonGroup>
              <Button
                icon="cube"
                text="Select Cube"
                onClick={this.handleCubesDialog}
              />
              <Button
                icon="folder-close"
                text="Open Query"
                onClick={this.handleOpenQueryDialog}
              />
            </ButtonGroup>
          }
        />

        {isOpenCubesDialog && <CubesDialog onClose={this.handleCubesDialog} />}
      </Fragment>
    ) : (
      !queryResultset.hasRun && (
        <Fragment>
          <NonIdealState
            icon="page-layout"
            title="Empty Workspace"
            action={
              <Button
                icon="edit"
                text="Query Designer"
                onClick={this.handleQueryDesignerDialog}
              />
            }
          />

          {isOpenQueryDesignerDialog && (
            <QueryDesignerDialog onClose={this.handleQueryDesignerDialog} />
          )}
        </Fragment>
      )
    );
  }
}

WorkspaceEmpty.propTypes = {
  query: PropTypes.object.isRequired,
  queryResultset: PropTypes.object.isRequired,
  openQuery: PropTypes.func.isRequired
};

export default WorkspaceEmpty;
