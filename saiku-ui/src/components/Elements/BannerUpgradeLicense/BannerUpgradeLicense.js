/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEqual from 'react-fast-compare';
import { Callout } from '@blueprintjs/core';

// UI
import { ExternalLink } from '../../UI';

// Utils
import { Links, Settings } from '../../../utils';

// Styles
import './BannerUpgradeLicense.css';

class BannerUpgradeLicense extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(this.props.license, nextProps.license);
  }

  dayDiff(currentDate, expirationDate) {
    return Math.round((expirationDate - currentDate) / (1000 * 60 * 60 * 24));
  }

  renderLicenseInfo() {
    const { expiration, licenseType } = this.props.license;
    const currentDate = new Date();
    const expirationDate = new Date(parseFloat(expiration));
    const remainingDays = this.dayDiff(currentDate, expirationDate);

    return licenseType !== Settings.LICENSE_TYPE.TRIAL &&
      licenseType !== Settings.LICENSE_TYPE.COMMUNITY ? (
      ''
    ) : licenseType === Settings.LICENSE_TYPE.TRIAL ? (
      <Callout className="sku-upgrade-header">
        You are using a Saiku Enterprise Trial license, you have {remainingDays}{' '}
        days remaining.{' '}
        <ExternalLink href={Links.saiku.license}>
          Buy licenses online.
        </ExternalLink>
      </Callout>
    ) : (
      <Callout className="sku-upgrade-header">
        You are using Saiku Community Edition, please consider upgrading to{' '}
        <ExternalLink href={Links.meteorite.home}>
          Saiku Enterprise
        </ExternalLink>
        ,{' '}
        <ExternalLink href={Links.meteorite.products.saiku.community}>
          or contribute by joining our community and helping other users!
        </ExternalLink>
      </Callout>
    );
  }

  render() {
    return this.renderLicenseInfo();
  }
}

BannerUpgradeLicense.propTypes = {
  license: PropTypes.object.isRequired
};

export default BannerUpgradeLicense;
