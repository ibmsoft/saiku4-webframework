/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NonIdealState } from '@blueprintjs/core';

// Elements
import Table from '../Table';
import Chart from '../Chart';

// Styles
import './WorkspaceResults.css';

class WorkspaceResults extends Component {
  getWorkspaceResultsHeight(renderMode) {
    const { isFullscreen } = this.props;
    let heightStyle = 'auto';

    if (renderMode === 'chart' && !isFullscreen) {
      heightStyle = '85%';
    } else if (renderMode === 'chart' && isFullscreen) {
      heightStyle = '100%';
    } else {
      heightStyle = 'auto';
    }

    return heightStyle;
  }

  renderError(queryResultset) {
    const { error } = queryResultset.queryResultset;

    return (
      <NonIdealState
        icon="error"
        title="Error"
        description={<span style={{ color: '#ee5342' }}>{error}</span>}
      />
    );
  }

  renderNoData(renderMode) {
    return (
      <NonIdealState
        icon={renderMode === 'table' ? 'th' : 'timeline-bar-chart'}
        title="No data to render"
      />
    );
  }

  render() {
    const { hasResultset, queryResultset } = this.props;
    const { workspace } = this.props.workspace;
    const { renderMode, chartType } = workspace;
    const workspaceResultsHeight = this.getWorkspaceResultsHeight(renderMode);

    return hasResultset ? (
      <div
        className="sku-workspace-results"
        style={{ height: workspaceResultsHeight }}
      >
        {renderMode === 'table' ? (
          <Table queryResultset={queryResultset} />
        ) : (
          <Chart type={chartType} queryResultset={queryResultset} />
        )}
      </div>
    ) : queryResultset.hasRun &&
      queryResultset.queryResultset.error !== null ? (
      this.renderError(queryResultset)
    ) : (
      this.renderNoData(renderMode)
    );
  }
}

WorkspaceResults.propTypes = {
  isFullscreen: PropTypes.bool,
  hasResultset: PropTypes.bool.isRequired,
  queryResultset: PropTypes.object.isRequired,
  workspace: PropTypes.object.isRequired
};

export default WorkspaceResults;
