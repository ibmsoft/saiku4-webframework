/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import axios from 'axios';
import qs from 'qs';

// Utils
import { Saiku, Settings } from '../utils';

// Constants
const REPOSITORY_URL = 'api/repository';

// Axios Cancellation
const CancelToken = axios.CancelToken;
let axiosCancelRequest;

class RepositoryService {
  static async getRepositories(type = 'saiku') {
    const url = `${Settings.REST_URL}/${REPOSITORY_URL}?type=${type}`;
    let response;

    try {
      response = await axios.get(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → RepositoryService.js → getRepositories()',
        error
      );
    }

    return response;
  }

  static async getDataFile(file) {
    const url = `${
      Settings.REST_URL
    }/${REPOSITORY_URL}/resource?file=${encodeURIComponent(file)}`;
    let response;

    try {
      response = await axios.get(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → RepositoryService.js → getDataFile()',
        error
      );
    }

    return response;
  }

  static async saveQuery(name, file, content) {
    const url = `${Settings.REST_URL}/${REPOSITORY_URL}/resource`;
    const data = {
      name,
      file,
      content
    };
    let response;

    try {
      response = await axios.post(url, qs.stringify(data), {
        headers: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → RepositoryService.js → saveQuery()',
        error
      );
    }

    return response;
  }

  static async getAclObjects(file) {
    const url = `${
      Settings.REST_URL
    }/${REPOSITORY_URL}/resource/acl?file=${encodeURIComponent(file)}`;
    let response;

    try {
      response = await axios.get(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → RepositoryService.js → getAclObjects()',
        error
      );
    }

    return response;
  }

  static async postAclObject(file, acl) {
    const url = `${Settings.REST_URL}/${REPOSITORY_URL}/resource/acl`;
    const data = {
      file,
      acl
    };
    let response;

    try {
      response = await axios.post(url, qs.stringify(data), {
        headers: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → RepositoryService.js → postAclObject()',
        error
      );
    }

    return response;
  }

  static async moveObject(source, target) {
    const url = `${Settings.REST_URL}/${REPOSITORY_URL}/resource/move`;
    const data = {
      source,
      target
    };
    let response;

    try {
      response = await axios.post(url, qs.stringify(data), {
        headers: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → RepositoryService.js → moveObject()',
        error
      );
    }

    return response;
  }

  static async deleteObject(file) {
    const url = `${
      Settings.REST_URL
    }/${REPOSITORY_URL}/resource?file=${encodeURIComponent(file)}`;
    let response;

    try {
      response = await axios.delete(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → RepositoryService.js → deleteObject()',
        error
      );

      return error.response;
    }

    return response;
  }

  static cancelRequest() {
    // Cancel the axios request
    // Reference: https://github.com/axios/axios#cancellation
    axiosCancelRequest();
  }
}

export default RepositoryService;
