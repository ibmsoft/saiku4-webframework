/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import axios from 'axios';

// Utils
import { Saiku, Settings } from '../utils';

// Constants
const USER_URL = 'admin/users';

// Axios Cancellation
const CancelToken = axios.CancelToken;
let axiosCancelRequest;

class UserService {
  static async getUsers() {
    const url = `${Settings.REST_URL}/${USER_URL}`;
    let response;

    try {
      response = await axios.get(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors('services → UserService.js → getUsers()', error);

      return error.response;
    }

    return response;
  }

  static async getUser(id) {
    const url = `${Settings.REST_URL}/${USER_URL}/${id}`;
    let response;

    try {
      response = await axios.get(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors('services → UserService.js → getUser()', error);

      return error.response;
    }

    return response;
  }

  static async addUser(data) {
    const url = `${Settings.REST_URL}/${USER_URL}`;
    let response;

    try {
      response = await axios.post(url, JSON.stringify(data), {
        headers: {
          'content-type': 'application/json'
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors('services → UserService.js → addUser()', error);

      return error.response;
    }

    return response;
  }

  static async updateUser(id, data) {
    const url = `${Settings.REST_URL}/${USER_URL}/${id}`;
    let response;

    try {
      response = await axios.put(url, JSON.stringify(data), {
        headers: {
          'content-type': 'application/json'
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → UserService.js → updateUser()',
        error
      );

      return error.response;
    }

    return response;
  }

  static async deleteUser(id) {
    const url = `${Settings.REST_URL}/${USER_URL}/${id}`;
    let response;

    try {
      response = await axios.delete(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → UserService.js → deleteUser()',
        error
      );

      return error.response;
    }

    return response;
  }

  static cancelRequest() {
    // Cancel the axios request
    // Reference: https://github.com/axios/axios#cancellation
    axiosCancelRequest();
  }
}

export default UserService;
