/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import axios from 'axios';
import fileDownload from 'js-file-download';

// Utils
import { Saiku, Settings } from '../utils';

// Constants
const SCHEMA_URL = 'admin/schema';

// Axios Cancellation
const CancelToken = axios.CancelToken;
let axiosCancelRequest;

class SchemaService {
  static async getSchemas() {
    const url = `${Settings.REST_URL}/${SCHEMA_URL}`;
    let response;

    try {
      response = await axios.get(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → SchemaService.js → getSchemas()',
        error
      );

      return error.response;
    }

    return response;
  }

  static async getDownloadSchemaFile(name) {
    const url = `${Settings.REST_URL}/${SCHEMA_URL}/${name}`;
    let response;

    try {
      response = await axios.get(url, {
        responseType: 'blob',
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → SchemaService.js → getDownloadSchemaFile()',
        error
      );

      return error.response;
    }

    fileDownload(response.data, name);

    return response;
  }

  static async postSchemaFile(file, name) {
    const url = `${Settings.REST_URL}/${SCHEMA_URL}/${name}`;
    const formData = new FormData();
    let response;

    formData.append('file', file);
    formData.append('name', name);

    try {
      response = await axios.post(url, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → SchemaService.js → postSchemaFile()',
        error
      );

      return error.response;
    }

    return response;
  }

  static async deleteSchema(name) {
    const url = `${Settings.REST_URL}/${SCHEMA_URL}/${name}`;
    let response;

    try {
      response = await axios.delete(url, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          axiosCancelRequest = c;
        })
      });
    } catch (error) {
      Saiku.axiosHandleErrors(
        'services → SchemaService.js → deleteSchema()',
        error
      );

      return error.response;
    }

    return response;
  }

  static cancelRequest() {
    // Cancel the axios request
    // Reference: https://github.com/axios/axios#cancellation
    axiosCancelRequest();
  }
}

export default SchemaService;
