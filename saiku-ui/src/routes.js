/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Views
import {
  AdminConsole,
  AutoLogin,
  GeneralSettings,
  Home,
  Login,
  QueryManager,
  Workspace
} from './components/Views';

// Utils
import { Settings } from './utils';

// Constants
const AUTO_LOGIN =
  Settings.AUTO_LOGIN.ACTIVE &&
  window.location.hostname &&
  window.location.hostname === Settings.AUTO_LOGIN.URL;

export const routes = [
  {
    path: '/',
    title: 'Login',
    exact: true,
    private: false,
    component: !AUTO_LOGIN ? Login : AutoLogin
  },
  {
    path: '/home',
    title: 'Home',
    exact: true,
    private: true,
    showSidebar: false,
    component: Home
  },
  {
    path: '/workspace',
    title: 'Workspace',
    exact: true,
    private: true,
    showSidebar: false,
    component: Workspace
  },
  {
    path: '/settings/general',
    title: 'General Settings',
    exact: true,
    private: true,
    showSidebar: true,
    component: GeneralSettings
  },
  {
    path: '/settings/querymanager',
    title: 'Query Manager',
    exact: true,
    private: true,
    showSidebar: true,
    component: QueryManager
  },
  {
    path: '/settings/adminconsole',
    title: 'Admin Console',
    exact: true,
    private: true,
    showSidebar: true,
    component: AdminConsole
  }
];
