/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

export default {
  meteorite: {
    home: 'https://www.meteorite.bi',
    products: {
      saiku: {
        home: 'https://www.meteorite.bi/products/saiku',
        pricing: 'https://www.meteorite.bi/products/saiku-pricing',
        sponsorship: 'https://www.meteorite.bi/products/saiku/sponsorship',
        community: 'https://www.meteorite.bi/products/saiku/community'
      }
    },
    services: {
      consulting: 'https://www.meteorite.bi/services/consulting'
    },
    email: {
      info: 'info@meteorite.bi'
    }
  },
  saiku: {
    home: 'https://saiku.meteorite.bi',
    docs: {
      home: 'https://docs.meteorite.bi',
      tutorials: 'https://docs.meteorite.bi/docs/tutorials/'
    },
    gitlab: 'https://gitlab.com/spiculedata/saiku4',
    license: 'https://license.meteorite.bi',
    twitter: 'https://twitter.com/SaikuAnalytics'
  }
};
