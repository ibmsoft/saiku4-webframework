/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import pkg from '../../package.json';

// Constants
const REST_URL =
  process.env.REACT_APP_STAGE === 'development'
    ? '/saiku/rest/saiku'
    : '/api/saiku/rest/saiku';

export const Settings = {
  VERSION: `Saiku-${pkg.version}`,
  BASE_URL: 'http://localhost:8080',
  TOMCAT_WEBAPP: '/saiku',
  REST_MOUNT_POINT: '/rest/saiku',
  REST_URL,
  COOKIE_NAME: 'SAIKU_TOKEN',
  COOKIE_EXPIRATION: 5, // 5 days
  EVALUATION_PANEL_LOGIN: true,
  MEASURES_GROUP_COLLAPSED: true,
  MEASURES_QUERY_DETAILS: {
    axis: 'COLUMNS',
    location: 'BOTTOM'
  },
  DIMENSION_SHOW_ALL: true,
  // Valid values for DIMENSION_HIDE_HIERARCHY:
  // 1) NONE
  // 2) SINGLE_LEVEL
  // 3) ALL
  DIMENSION_HIDE_HIERARCHY: 'SINGLE_LEVEL',
  QUERY_PROPERTIES: {
    'saiku.olap.query.automatic_execution': true,
    'saiku.olap.query.nonempty': true,
    'saiku.olap.query.nonempty.rows': true,
    'saiku.olap.query.nonempty.columns': true,
    'saiku.ui.render.mode': 'table',
    'saiku.olap.query.filter': true,
    'saiku.olap.result.formatter': 'flattened'
  },
  TABLE_LAZY: true, // Turn lazy loading off / on
  TABLE_LAZY_SIZE: 1000, // Initial number of items to be rendered
  TABLE_LAZY_LOAD_ITEMS: 20, // Additional item per scroll
  TABLE_LAZY_LOAD_TIME: 20, // Throttling call of lazy loading items
  // Valid values for CELLSET_FORMATTER:
  // 1) flattened
  // 2) flat
  CELLSET_FORMATTER: 'flattened',
  MEMBERS_FROM_RESULT: true,
  MEMBERS_LIMIT: 3000,
  MEMBERS_SEARCH_LIMIT: 75,
  CHART_COLORS: [
    '#1f77b4',
    '#aec7e8',
    '#ff7f0e',
    '#ffbb78',
    '#2ca02c',
    '#98df8a',
    '#d62728',
    '#ff9896',
    '#9467bd',
    '#c5b0d5',
    '#8c564b',
    '#c49c94',
    '#e377c2',
    '#f7b6d2',
    '#7f7f7f',
    '#c7c7c7',
    '#bcbd22',
    '#dbdb8d',
    '#17becf',
    '#9edae5'
  ],
  SPLASH_SCREEN_TIME: 2000,
  SENTRY_DSN_KEY:
    'https://a09e15d601ff4d68ac0b6e4fcee14434@sentry.meteorite.bi/2',
  SAIKU_COLOR: '#c52120',
  QUERY_PROPERTY_DATA: 'saiku4.ui.workspace.data',
  DEFAULT_ROUTE: '/home',
  DEFAULT_FILE_NAME: 'Unsaved query',
  DEFAULT_RENDER_TYPE: 'table',
  DEFAULT_CHART_TYPE: 'stackedBar',
  LOGIN_ROLE_ADMIN: {
    username: 'admin',
    password: 'admin'
  },
  LOGIN_ROLE_USER: {
    username: 'demo',
    password: 'demo'
  },
  SAIKU_DEMO_URL: 'try.meteorite.bi',
  SAIKU_DEMO_LOGIN: {
    username: 'demo',
    password: 'demo'
  },
  AUTO_LOGIN: {
    ACTIVE: true,
    URL: 'try.meteorite.bi',
    USERNAME: 'demo',
    PASSWORD: 'demo'
  },
  PLUGINS: {
    QUERY_DESIGNER: {
      SELECT_NAVIGATION_TAB: 'measures' // measures or dimensions
    }
  },
  LICENSE_TYPE: {
    COMMUNITY: 'Saiku 4 Community Edition',
    TRIAL: 'Saiku 4 Enterprise Edition - Trial',
    FULL: 'Saiku 4 Enterprise Edition'
  },
  JDBC: {
    URL: [
      'jdbc:mysql://<server>:<port>/<databaseName>',
      'jdbc:postgresql://<server>:<port>/<databaseName>',
      'jdbc:oracle:thin:@<server>:<port>:<databaseName>',
      'jdbc:drill:zk=<zk name>[:<port>][,<zk name2>[:<port>]',
      'jdbc:jtds:sqlserver://<server>:<port>/<databaseName>'
    ],
    DRIVER: [
      // Reference: https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-api-changes.html
      // 'com.mysql.jdbc.Driver', is deprecated
      'com.mysql.cj.jdbc.Driver',
      'org.postgresql.Driver',
      'oracle.jdbc.OracleDriver',
      'org.apache.drill.jdbc.Driver',
      'net.sourceforge.jtds.jdbc.Driver'
    ]
  },
  LANGUAGES: ['GB']
};

Settings.PARAMS = (() => {
  const params = {};

  Object.keys(Settings).forEach(key => {
    if (key.match('^PARAM') === 'PARAM') {
      params[key] = Settings[key];
    }
  });

  return params;
})();

Settings.getCookieExpires = timestamp => {
  const expire = new Date(timestamp * 1000);

  return expire;
};
