/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

// Packages
import uuid from 'uuid/v4';

// Types
import {
  SAVE_SELECTED_CUBE,
  SAVE_QUERY,
  SAVE_OPEN_QUERY,
  SAVE_QUERY_DESIGNER,
  SWITCH_RENDER,
  RESET_QUERY,
  RESET_STATE
} from '../actions/types';

// Utils
import { Settings } from '../utils';

const initialState = {
  workspace: {
    id: uuid(),
    selectedCube: null, // e.g: foodmart/FoodMart/FoodMart/Sales
    file: null, // e.g: /homes/home:admin/report.saiku
    viewState: 'NEW_QUERY', // NEW_QUERY, EDIT_QUERY or OPEN_QUERY
    renderMode: 'table', // table or chart
    chartType: null, // bar, stackedBar, multiplebar and etc
    plugins: {
      queryDesigner: {
        measuresData: [],
        dimensionsData: [],
        axes: {
          MEASURES: {
            id: 'MEASURES',
            title: 'Measures',
            items: []
          },
          ROWS: {
            id: 'ROWS',
            title: 'Rows',
            items: []
          },
          COLUMNS: {
            id: 'COLUMNS',
            title: 'Columns',
            items: []
          },
          FILTER: {
            id: 'FILTER',
            title: 'Filter',
            items: []
          }
        },
        measuresDetails: Settings.MEASURES_QUERY_DETAILS
      }
    }
  },
  loading: false
};

export const workspaceReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_SELECTED_CUBE:
      return {
        ...state,
        workspace: {
          ...state.workspace,
          selectedCube: action.payload
        }
      };
    case SAVE_QUERY:
      const { file } = action.payload;

      return {
        ...state,
        workspace: {
          ...state.workspace,
          file
        }
      };
    case SAVE_OPEN_QUERY:
      return {
        ...state,
        workspace: action.payload ? action.payload : initialState.workspace
      };
    case SAVE_QUERY_DESIGNER:
      return {
        ...state,
        workspace: {
          ...state.workspace,
          viewState: 'EDIT_QUERY',
          plugins: {
            ...state.workspace.plugins,
            queryDesigner: action.payload
          }
        }
      };
    case SWITCH_RENDER:
      const { renderMode, chartType } = action.payload;

      return {
        ...state,
        workspace: {
          ...state.workspace,
          renderMode,
          chartType
        }
      };
    case RESET_QUERY:
    case RESET_STATE:
      return initialState;
    default:
      return state;
  }
};
