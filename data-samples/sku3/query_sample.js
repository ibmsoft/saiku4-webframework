/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

{
  name: '417F9124-2C62-A54E-8B08-742ABB9DE93F',
  queryModel: {
    axes: {
      FILTER: {
        mdx: null,
        filters: [],
        sortOrder: null,
        sortEvaluationLiteral: null,
        hierarchizeMode: null,
        location: 'FILTER',
        hierarchies: [],
        nonEmpty: false,
        aggregators: []
      },
      COLUMNS: {
        mdx: null,
        filters: [],
        sortOrder: null,
        sortEvaluationLiteral: null,
        hierarchizeMode: null,
        location: 'COLUMNS',
        hierarchies: [
          {
            name: '[Product].[Products]',
            levels: {
              'Product Family': {
                name: 'Product Family'
              }
            },
            cmembers: {}
          }
        ],
        nonEmpty: true,
        aggregators: []
      },
      ROWS: {
        mdx: null,
        filters: [],
        sortOrder: null,
        sortEvaluationLiteral: null,
        hierarchizeMode: null,
        location: 'ROWS',
        hierarchies: [
          {
            mdx: null,
            filters: [],
            sortOrder: null,
            sortEvaluationLiteral: null,
            hierarchizeMode: null,
            name: '[Time].[Time]',
            caption: 'Time',
            dimension: 'Time',
            levels: {
              Year: {
                mdx: null,
                filters: [],
                name: 'Year',
                caption: 'Year',
                selection: {
                  type: 'INCLUSION',
                  members: [],
                  parameterName: null
                },
                aggregators: [],
                measureAggregators: []
              }
            },
            cmembers: {}
          }
        ],
        nonEmpty: true,
        aggregators: []
      }
    },
    visualTotals: false,
    visualTotalsPattern: null,
    lowestLevelsOnly: false,
    details: {
      axis: 'COLUMNS',
      location: 'BOTTOM',
      measures: [
        {
          name: 'Unit Sales',
          uniqueName: '[Measures].[Unit Sales]',
          caption: 'Unit Sales',
          type: 'EXACT',
          aggregators: []
        }
      ]
    },
    calculatedMeasures: [],
    calculatedMembers: []
  },
  cube: {
    uniqueName: '[foodmart].[FoodMart].[FoodMart].[Sales]',
    name: 'Sales',
    connection: 'foodmart',
    catalog: 'FoodMart',
    schema: 'FoodMart',
    caption: null,
    visible: false
  },
  mdx: 'WITH\nSET [~ROWS] AS\n    {[Time].[Time].[Year].Members}\nSELECT\nNON EMPTY {[Measures].[Unit Sales]} ON COLUMNS,\nNON EMPTY [~ROWS] ON ROWS\nFROM [Sales]',
  parameters: {},
  plugins: {},
  properties: {
    'saiku.olap.query.automatic_execution': true,
    'saiku.olap.query.nonempty': true,
    'saiku.olap.query.nonempty.rows': true,
    'saiku.olap.query.nonempty.columns': true,
    'saiku.ui.render.mode': 'table',
    'saiku.olap.query.filter': true,
    'saiku.olap.result.formatter': 'flattened',
    'org.saiku.query.explain': true,
    'saiku.olap.query.drillthrough': true,
    'org.saiku.connection.scenario': false
  },
  metadata: {},
  queryType: 'OLAP',
  type: 'QUERYMODEL'
}
