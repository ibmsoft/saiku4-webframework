/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

{
  name: '19B03AEC-6375-E814-8830-2A6E89329603',
  queryModel: {
    axes: {
      FILTER: {
        mdx: null,
        filters: [],
        sortOrder: null,
        sortEvaluationLiteral: null,
        hierarchizeMode: null,
        location: 'FILTER',
        hierarchies: [],
        nonEmpty: false,
        aggregators: []
      },
      COLUMNS: {
        mdx: null,
        filters: [],
        sortOrder: null,
        sortEvaluationLiteral: null,
        hierarchizeMode: null,
        location: 'COLUMNS',
        hierarchies: [],
        nonEmpty: true,
        aggregators: []
      },
      ROWS: {
        mdx: null,
        filters: [],
        sortOrder: null,
        sortEvaluationLiteral: null,
        hierarchizeMode: null,
        location: 'ROWS',
        hierarchies: [
          {
            mdx: null,
            filters: [],
            sortOrder: null,
            sortEvaluationLiteral: null,
            hierarchizeMode: null,
            name: '[Product].[Products]',
            caption: 'Products',
            dimension: 'Product',
            levels: {
              'Product Family': {
                mdx: null,
                filters: [],
                name: 'Product Family',
                caption: 'Product Family',
                selection: {
                  type: 'INCLUSION',
                  members: [],
                  parameterName: null
                },
                aggregators: [],
                measureAggregators: []
              },
              'Product Department': {
                mdx: null,
                filters: [],
                name: 'Product Department',
                caption: 'Product Department',
                selection: {
                  type: 'INCLUSION',
                  members: [],
                  parameterName: null
                },
                aggregators: [],
                measureAggregators: []
              }
            },
            cmembers: {}
          }
        ],
        nonEmpty: true,
        aggregators: []
      }
    },
    visualTotals: false,
    visualTotalsPattern: null,
    lowestLevelsOnly: false,
    details: {
      axis: 'COLUMNS',
      location: 'BOTTOM',
      measures: [
        {
          name: 'Unit Sales',
          uniqueName: '[Measures].[Unit Sales]',
          caption: 'Unit Sales',
          type: 'EXACT',
          aggregators: []
        },
        {
          name: 'Store Cost',
          uniqueName: '[Measures].[Store Cost]',
          caption: 'Store Cost',
          type: 'EXACT',
          aggregators: []
        },
        {
          name: 'Store Sales',
          uniqueName: '[Measures].[Store Sales]',
          caption: 'Store Sales',
          type: 'EXACT',
          aggregators: []
        }
      ]
    },
    calculatedMeasures: [],
    calculatedMembers: []
  },
  queryType: 'OLAP',
  type: 'QUERYMODEL',
  cube: {
    uniqueName: '[foodmart].[FoodMart].[FoodMart].[Sales]',
    name: 'Sales',
    connection: 'foodmart',
    catalog: 'FoodMart',
    schema: 'FoodMart',
    caption: null,
    visible: false
  },
  mdx: 'WITH\nSET [~ROWS] AS\n    Hierarchize({{[Product].[Products].[Product Family].Members}, {[Product].[Products].[Product Department].Members}})\nSELECT\nNON EMPTY {[Measures].[Unit Sales], [Measures].[Store Cost], [Measures].[Store Sales]} ON COLUMNS,\nNON EMPTY [~ROWS] ON ROWS\nFROM [Sales]',
  parameters: {},
  plugins: {},
  properties: {
    'saiku.olap.query.automatic_execution': true,
    'saiku.olap.query.nonempty': true,
    'saiku.olap.query.nonempty.rows': true,
    'saiku.olap.query.nonempty.columns': true,
    'saiku.ui.render.mode': 'table',
    'saiku.olap.query.filter': true,
    'saiku.olap.result.formatter': 'flattened',
    'org.saiku.query.explain': true,
    'saiku.olap.query.drillthrough': true,
    'org.saiku.connection.scenario': false
  },
  metadata: {}
}
