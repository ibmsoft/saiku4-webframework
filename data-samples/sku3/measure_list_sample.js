/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

[
  {
    uniqueName: '[Measures].[Unit Sales]',
    name: 'Unit Sales',
    caption: 'Unit Sales',
    dimensionUniqueName: '[Measures]',
    description: null,
    levelUniqueName: '[Measures].[MeasuresLevel]',
    hierarchyUniqueName: '[Measures]',
    calculated: false,
    measureGroup: 'Sales'
  },
  {
    uniqueName: '[Measures].[Store Cost]',
    name: 'Store Cost',
    caption: 'Store Cost',
    dimensionUniqueName: '[Measures]',
    description: null,
    levelUniqueName: '[Measures].[MeasuresLevel]',
    hierarchyUniqueName: '[Measures]',
    calculated: false,
    measureGroup: 'Sales'
  },
  {
    uniqueName: '[Measures].[Store Sales]',
    name: 'Store Sales',
    caption: 'Store Sales',
    dimensionUniqueName: '[Measures]',
    description: null,
    levelUniqueName: '[Measures].[MeasuresLevel]',
    hierarchyUniqueName: '[Measures]',
    calculated: false,
    measureGroup: 'Sales'
  },
  {
    uniqueName: '[Measures].[Sales Count]',
    name: 'Sales Count',
    caption: 'Sales Count',
    dimensionUniqueName: '[Measures]',
    description: null,
    levelUniqueName: '[Measures].[MeasuresLevel]',
    hierarchyUniqueName: '[Measures]',
    calculated: false,
    measureGroup: 'Sales'
  },
  {
    uniqueName: '[Measures].[Customer Count]',
    name: 'Customer Count',
    caption: 'Customer Count',
    dimensionUniqueName: '[Measures]',
    description: null,
    levelUniqueName: '[Measures].[MeasuresLevel]',
    hierarchyUniqueName: '[Measures]',
    calculated: false,
    measureGroup: 'Sales'
  },
  {
    uniqueName: '[Measures].[Promotion Sales]',
    name: 'Promotion Sales',
    caption: 'Promotion Sales',
    dimensionUniqueName: '[Measures]',
    description: null,
    levelUniqueName: '[Measures].[MeasuresLevel]',
    hierarchyUniqueName: '[Measures]',
    calculated: false,
    measureGroup: 'Sales'
  },
  {
    uniqueName: '[Measures].[Profit]',
    name: 'Profit',
    caption: 'Profit',
    dimensionUniqueName: '[Measures]',
    description: null,
    levelUniqueName: '[Measures].[MeasuresLevel]',
    hierarchyUniqueName: '[Measures]',
    calculated: true,
    measureGroup: ''
  },
  {
    uniqueName: '[Measures].[Profit Growth]',
    name: 'Profit Growth',
    caption: 'Gewinn-Wachstum',
    dimensionUniqueName: '[Measures]',
    description: null,
    levelUniqueName: '[Measures].[MeasuresLevel]',
    hierarchyUniqueName: '[Measures]',
    calculated: true,
    measureGroup: ''
  }
]
