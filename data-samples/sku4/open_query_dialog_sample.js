/*
 *   Copyright 2012-present OSBI Ltd
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

[
  {
    '@class': 'org.saiku.repository.RepositoryFolderObject',
    type: 'FOLDER',
    hasCaret: true,
    icon: 'folder-close',
    label: 'datasources',
    id: '#/datasources',
    path: '/datasources',
    childNodes: [],
    acl: [
      'READ',
      'WRITE',
      'GRANT'
    ]
  },
  {
    '@class': 'org.saiku.repository.RepositoryFolderObject',
    type: 'FOLDER',
    hasCaret: true,
    icon: 'folder-close',
    label: 'etc',
    id: '#/etc',
    path: '/etc',
    childNodes: [
      {
        '@class': 'org.saiku.repository.RepositoryFolderObject',
        type: 'FOLDER',
        hasCaret: true,
        icon: 'folder-close',
        label: 'legacyreports',
        id: '#/etc/legacyreports',
        path: '/etc/legacyreports',
        childNodes: [],
        acl: [
          'READ',
          'WRITE',
          'GRANT'
        ]
      },
      {
        '@class': 'org.saiku.repository.RepositoryFolderObject',
        type: 'FOLDER',
        hasCaret: true,
        icon: 'folder-close',
        label: 'theme',
        id: '#/etc/theme',
        path: '/etc/theme',
        childNodes: [
          {
            '@class': 'org.saiku.repository.RepositoryFolderObject',
            type: 'FOLDER',
            hasCaret: true,
            icon: 'folder-close',
            label: 'legacyreports',
            id: '#/etc/theme/legacyreports',
            path: '/etc/theme/legacyreports',
            childNodes: [],
            acl: [
              'READ',
              'WRITE',
              'GRANT'
            ]
          }
        ],
        acl: [
          'READ',
          'WRITE',
          'GRANT'
        ]
      }
    ],
    acl: [
      'READ',
      'WRITE',
      'GRANT'
    ]
  },
  {
    '@class': 'org.saiku.repository.RepositoryFolderObject',
    type: 'FOLDER',
    hasCaret: true,
    icon: 'folder-close',
    label: 'homes',
    id: '#/homes',
    path: '/homes',
    childNodes: [
      {
        '@class': 'org.saiku.repository.RepositoryFolderObject',
        type: 'FOLDER',
        hasCaret: true,
        icon: 'folder-close',
        label: 'home:admin',
        id: '#/homes/home:admin',
        path: '/homes/home:admin',
        childNodes: [
          {
            '@class': 'org.saiku.repository.RepositoryFolderObject',
            type: 'FOLDER',
            hasCaret: true,
            icon: 'folder-close',
            label: 'sample_reports',
            id: '#/homes/home:admin/sample_reports',
            path: '/homes/home:admin/sample_reports',
            childNodes: [
              {
                '@class': 'org.saiku.repository.RepositoryFileObject',
                type: 'FILE',
                icon: 'document',
                label: 'average_mag_and_depth_over_time.saiku',
                id: '#/homes/home:admin/sample_reports/average_mag_and_depth_over_time.saiku',
                path: '/homes/home:admin/sample_reports/average_mag_and_depth_over_time.saiku',
                acl: [
                  'READ',
                  'WRITE',
                  'GRANT'
                ],
                fileType: 'saiku'
              },
              {
                '@class': 'org.saiku.repository.RepositoryFileObject',
                type: 'FILE',
                icon: 'document',
                label: 'average_magnitude_with_quakes.saiku',
                id: '#/homes/home:admin/sample_reports/average_magnitude_with_quakes.saiku',
                path: '/homes/home:admin/sample_reports/average_magnitude_with_quakes.saiku',
                acl: [
                  'READ',
                  'WRITE',
                  'GRANT'
                ],
                fileType: 'saiku'
              },
              {
                '@class': 'org.saiku.repository.RepositoryFileObject',
                type: 'FILE',
                icon: 'document',
                label: 'filtered_network_example.saiku',
                id: '#/homes/home:admin/sample_reports/filtered_network_example.saiku',
                path: '/homes/home:admin/sample_reports/filtered_network_example.saiku',
                acl: [
                  'READ',
                  'WRITE',
                  'GRANT'
                ],
                fileType: 'saiku'
              },
              {
                '@class': 'org.saiku.repository.RepositoryFileObject',
                type: 'FILE',
                icon: 'document',
                label: 'number_of_quakes_over_time.saiku',
                id: '#/homes/home:admin/sample_reports/number_of_quakes_over_time.saiku',
                path: '/homes/home:admin/sample_reports/number_of_quakes_over_time.saiku',
                acl: [
                  'READ',
                  'WRITE',
                  'GRANT'
                ],
                fileType: 'saiku'
              },
              {
                '@class': 'org.saiku.repository.RepositoryFileObject',
                type: 'FILE',
                icon: 'document',
                label: 'param_example.saiku',
                id: '#/homes/home:admin/sample_reports/param_example.saiku',
                path: '/homes/home:admin/sample_reports/param_example.saiku',
                acl: [
                  'READ',
                  'WRITE',
                  'GRANT'
                ],
                fileType: 'saiku'
              },
              {
                '@class': 'org.saiku.repository.RepositoryFileObject',
                type: 'FILE',
                icon: 'document',
                label: 'yoy_average_depth_vs_max_depth.saiku',
                id: '#/homes/home:admin/sample_reports/yoy_average_depth_vs_max_depth.saiku',
                path: '/homes/home:admin/sample_reports/yoy_average_depth_vs_max_depth.saiku',
                acl: [
                  'READ',
                  'WRITE',
                  'GRANT'
                ],
                fileType: 'saiku'
              }
            ],
            acl: [
              'READ',
              'WRITE',
              'GRANT'
            ]
          },
          {
            '@class': 'org.saiku.repository.RepositoryFileObject',
            type: 'FILE',
            icon: 'document',
            label: 'new_report.saiku',
            id: '#/homes/home:admin/new_report.saiku',
            path: '/homes/home:admin/new_report.saiku',
            acl: [
              'READ',
              'WRITE',
              'GRANT'
            ],
            fileType: 'saiku'
          },
          {
            '@class': 'org.saiku.repository.RepositoryFileObject',
            type: 'FILE',
            icon: 'document',
            label: 'report_one.saiku',
            id: '#/homes/home:admin/report_one.saiku',
            path: '/homes/home:admin/report_one.saiku',
            acl: [
              'READ',
              'WRITE',
              'GRANT'
            ],
            fileType: 'saiku'
          },
          {
            '@class': 'org.saiku.repository.RepositoryFileObject',
            type: 'FILE',
            icon: 'document',
            label: 'report_two.saiku',
            id: '#/homes/home:admin/report_two.saiku',
            path: '/homes/home:admin/report_two.saiku',
            acl: [
              'READ',
              'WRITE',
              'GRANT'
            ],
            fileType: 'saiku'
          },
          {
            '@class': 'org.saiku.repository.RepositoryFileObject',
            type: 'FILE',
            icon: 'document',
            label: 'test.saiku',
            id: '#/homes/home:admin/test.saiku',
            path: '/homes/home:admin/test.saiku',
            acl: [
              'READ',
              'WRITE',
              'GRANT'
            ],
            fileType: 'saiku'
          }
        ],
        acl: [
          'READ',
          'WRITE',
          'GRANT'
        ]
      },
      {
        '@class': 'org.saiku.repository.RepositoryFolderObject',
        type: 'FOLDER',
        hasCaret: true,
        icon: 'folder-close',
        label: 'home:smith',
        id: '#/homes/home:smith',
        path: '/homes/home:smith',
        childNodes: [],
        acl: [
          'READ',
          'WRITE',
          'GRANT'
        ]
      }
    ],
    acl: [
      'READ',
      'WRITE',
      'GRANT'
    ]
  }
]
