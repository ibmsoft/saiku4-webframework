# [Saiku Analytics 4](https://saiku.meteorite.bi/)

> The world's greatest open source OLAP browser

[![saiku4-demo](./media-kit/demo/demo_02.png)](https://try.meteorite.bi/)

### **Try the demo** → https://try.meteorite.bi

---

Saiku allows business users to explore complex data sources, using a familiar drag and drop interface and easy to understand business terminology, all within a browser. Select the data you are interested in, look at it from different perspectives, drill into the detail. Once you have your answer, save your results, share them, export them to Excel or PDF, all straight from the browser. [(more)](https://saiku.meteorite.bi/)

---

## Quick Start

First of all, install the dependencies to run this app.

- [NodeJS](http://nodejs.org/)
- [Yarn](https://yarnpkg.com/lang/en/docs/cli/install/) - _Optional_

```bash
# Install dependencies for server
$ yarn install (or npm i)

# Install dependencies for app
$ yarn app-install (or npm run app-install)

# Run the app & server with concurrently
$ yarn dev (or npm run dev)

# Run the Node.js proxy server only
$ yarn server (or npm run server)

# Run the Saiku app only
$ yarn app (or npm run app)

# Server runs on http://localhost:9999 and app on http://localhost:3000
```

## Development

#### Saiku Server

**Docker:**

The quickest way to get Saiku up and running is by using our preconfigured Docker image.

```bash
$ docker run -p 80:80 meteorite/saikuserver:latest
```

For the app to work in development mode you must have the Saiku Server running at `http://localhost`.

## Build

Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance.

```bash
# Builds the app
$ yarn build (or npm run build)
```

> The `build` folder will be inside the `saiku-ui` folder.

## Learning Resources

If you're new to [React](https://reactjs.org/), you should have a basic understanding of how it works before jumping into this project. Mainly, you should understand what `JSX` is, the difference between a functional/stateless and class component, and how `setState` works inside class components.

Here are some resources for learning basic React:

- https://reactjs.org/tutorial/tutorial.html
- https://reactforbeginners.com

Once you're familiar with React, you should also begin to understand what [Redux](https://redux.js.org/) is. For Redux, you should understand what an action creator and a reducer is, how you `connect` a component to the Redux store using [react-redux](https://github.com/reduxjs/react-redux) (and using connect's 2 arguments, `mapStateToProps` and `bindActionCreators`), and when to use Redux vs. local component state.

Good resources for learning Redux:

- https://redux.js.org
- https://egghead.io/courses/getting-started-with-redux
- https://learnredux.com

A few other things you will want to understand that we make heavy use of in this app:

- [Redux Thunk](https://github.com/reduxjs/redux-thunk) - A "middleware" library for redux that lets us dispatch a function instead of a plain object action. Helps with doing async stuff in actions.
- [React Router](https://github.com/ReactTraining/react-router) - This is the router we use for the entire application. We add new routes using declarative config but for things like `Link` and `props.location.match` you'll need to understand how RR works on some level.

## Screenshots

| <img src="./media-kit/demo/demo_01.png" width="440" height="247"> | <img src="./media-kit/demo/demo_02.png" width="440" height="247"> |
| :---------------------------------------------------------------: | :---------------------------------------------------------------: |
|                          **Login Page**                           |                          **Home Page**                            |

| <img src="./media-kit/demo/demo_03.png" width="440" height="247"> | <img src="./media-kit/demo/demo_04.png" width="440" height="247"> |
| :---------------------------------------------------------------: | :---------------------------------------------------------------: |
|                        **Query Designer**                         |                       **Member Selection**                        |

| <img src="./media-kit/demo/demo_05.png" width="440" height="247"> | <img src="./media-kit/demo/demo_06.png" width="440" height="247"> |
| :---------------------------------------------------------------: | :---------------------------------------------------------------: |
|                          **Table Mode**                           |                          **Chart Mode**                           |

| <img src="./media-kit/demo/demo_07.png" width="440" height="247"> | <img src="./media-kit/demo/demo_08.png" width="440" height="247"> |
| :---------------------------------------------------------------: | :---------------------------------------------------------------: |
|                         **Chart Options**                         |                           **MDX Code**                            |

## License

Saiku Analytics 4 is free software. The UI, contained in this repository, is available under the terms of the Apache License Version 2. A copy is attached for your convenience.
